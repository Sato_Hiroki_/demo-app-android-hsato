package com.example.mydemoapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.list_item.*
import com.example.mydemoapp.R
class MainActivity : AppCompatActivity() {

    var TextList: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addList()

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ListAdapter(TextList, object : com.example.mydemoapp.ListAdapter.OnItemClickListener {

            override fun onItemClickListener(view: View, position: Int, Text: String) {
                when (position) {
                    0 -> {
                        val intent = Intent(this@MainActivity, PickerActivity::class.java)
                        startActivity(intent)
                    }
                    1 -> {
                        val intent = Intent(this@MainActivity, MapsActivity::class.java)
                        startActivity(intent)
                    }
                    2 -> {
                        val intent = Intent(this@MainActivity, VideoActivity::class.java)
                        startActivity(intent)
                    }
                    3 -> {
                        val intent = Intent(this@MainActivity, WebViewActivity::class.java)
                        startActivity(intent)
                    }
                    4 -> {
                        val intent = Intent(this@MainActivity, ViewPagerActivity::class.java)
                        startActivity(intent)
                    }
                    5 -> {
                        val intent = Intent(this@MainActivity, FormActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        })
    }

    fun addList() {
        TextList.add("Picker")
        TextList.add("Map")
        TextList.add("Video")
        TextList.add("Web View")
        TextList.add("View Pager")
        TextList.add("Form")
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}