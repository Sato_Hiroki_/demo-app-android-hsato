package com.example.mydemoapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_form2.*

class Form2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form2)
        supportActionBar?.title = "アンケート"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val genderIndex = intent.getStringExtra("gender")
        val tvSubGenderIndex = findViewById<TextView>(R.id.S_textView)
        tvSubGenderIndex.text = genderIndex

        val yearIndex = intent.getStringExtra("YEAR")
        val tvSubYearIndex = findViewById<TextView>(R.id.B_textView)
        tvSubYearIndex.text = yearIndex

        val monthIndex = intent.getStringExtra("MON")
        val tvSubMonthIndex = findViewById<TextView>(R.id.MONTH_textView)
        tvSubMonthIndex.text = monthIndex

        val dayIndex = intent.getStringExtra("DAY")
        val tvSubDayIndex = findViewById<TextView>(R.id.DAY_textView)
        tvSubDayIndex.text = dayIndex

        val Inernet = intent.getStringExtra("INTERNET")
        val tvSubInter = findViewById<TextView>(R.id.inter_ans)
        tvSubInter.text = Inernet

        val News = intent.getStringExtra("NEWS")
        val tvSubNews = findViewById<TextView>(R.id.news_ans)
        tvSubNews.text = News

        val Friend = intent.getStringExtra("FRIEND")
        val tvSubFriend = findViewById<TextView>(R.id.friend_ans)
        tvSubFriend.text = Friend

        val Semina = intent.getStringExtra("SEMINA")
        val tvSubSemi = findViewById<TextView>(R.id.semi_ans)
        tvSubSemi.text = Semina

        val Other = intent.getStringExtra("OTHER")
        val tvSubOther = findViewById<TextView>(R.id.other_ans)
        tvSubOther.text = Other

        N_textView.text = intent.getStringExtra(FormActivity.NAME)
        A_textView.text = intent.getStringExtra(FormActivity.AGE)

        Back_button.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}






