package com.example.mydemoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_view_pager.*

class ViewPagerActivity : AppCompatActivity() {

    val myView : Array<Int> = arrayOf(R.layout.lay_one,
        R.layout.lay_two,
        R.layout.lay_three)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.title="ViewPager"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_view_pager)

        view_pager.adapter = VpAdapter(myView, this@ViewPagerActivity)

    }

    override fun onSupportNavigateUp(): Boolean{
        finish()
        return super.onSupportNavigateUp()
    }
}
