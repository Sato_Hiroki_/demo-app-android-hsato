package com.example.mydemoapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item.view.*

class ListAdapter(private val TextList: ArrayList<String>, val listener: OnItemClickListener) :
    RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    class ViewHolder(val textView: View) : RecyclerView.ViewHolder(textView)

    private lateinit var onItemClickListener: OnItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        setOnItemClickListener(listener)
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.textView.text = TextList[position]
        holder.textView.setOnClickListener{
            listener.onItemClickListener(it, position, TextList[position])
        }
    }

    override fun getItemCount(): Int {
        return TextList.size
    }

    interface OnItemClickListener{
        fun onItemClickListener(view: View, position: Int, Text: String)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }
}