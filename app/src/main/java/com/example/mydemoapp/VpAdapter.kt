package com.example.mydemoapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter

class VpAdapter (private val theViews: Array<Int>,
                 private val theContext: Context
) : PagerAdapter() {

    override fun isViewFromObject(
        view: View,
        `object`: Any
    ): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return theViews.size
    }

    override fun instantiateItem(
        container: ViewGroup,
        position: Int
    ): Any {

        val thisView = theViews[position]
        val inflater = LayoutInflater.from(theContext)

        val layout = inflater.inflate(
            thisView, container,
            false
        ) as ViewGroup

        container.addView(layout)
        return layout
    }

    override fun destroyItem(
        container: ViewGroup,
        position: Int,
        view: Any
    ) {
        container.removeView(view as View)
    }
}
